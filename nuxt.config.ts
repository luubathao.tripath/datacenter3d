import UnpluginComponentsVite from 'unplugin-vue-components/vite'
import IconsResolver from 'unplugin-icons/resolver'
import vitePluginString from 'vite-plugin-string'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
  // server side rendering mode
  ssr: false,
  // head: {
  //   script: [
  //     {
  //       type: 'text/javascript',
  //       src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDbFKNP0PF35sv9ROGzdVryTPIDiQcODBA&callback=initMap"',
  //       defer: true,
  //       async: true,
  //     },
  //   ],
  // },

  // typescripts
  typescript: {
    strict: true,
    typeCheck: true,
  },

  // css
  css: [
    'virtual:windi-base.css',
    'virtual:windi-components.css',
    'virtual:windi-utilities.css',
    '~/assets/sass/vendor.scss',
    '~/assets/sass/app.scss',
  ],

  // plugins
  plugins: ['~/plugins/navbar.ts'],

  // build
  build: {
    transpile: ['@headlessui/vue'],
  },

  // modules
  modules: ['@pinia/nuxt', '@vueuse/nuxt', 'nuxt-windicss'],

  // experimental features
  experimental: {
    reactivityTransform: false,
  },

  // auto import components
  components: true,

  // vite plugins
  vite: {
    plugins: [
      UnpluginComponentsVite({
        dts: true,
        resolvers: [
          IconsResolver({
            prefix: 'Icon',
          }),
        ],
      }),
      vitePluginString(),
    ],
  },

  // localization - i18n config
  // intlify: {
  //   localeDir: 'locales',
  //   vueI18n: {
  //     locale: 'en',
  //     fallbackLocale: 'en',
  //     availableLocales: ['en', 'id', 'ja', 'ko'],
  //   },
  // },

  // vueuse
  vueuse: {
    ssrHandlers: false,
  },

  // windicss
  // windicss: {
  //   analyze: {
  //     analysis: {
  //       interpretUtilities: false,
  //     },
  //     server: {
  //       port: 4000,
  //       open: false,
  //     },
  //   },
  //   scan: true,
  // },

  // content
  // content: {
  //   documentDriven: true,
  //   markdown: {
  //     mdc: true,
  //   },
  //   highlight: {
  //     theme: 'github-dark',
  //   },
  // },
})
