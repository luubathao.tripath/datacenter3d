uniform sampler2D globeTexture;

varying vec2 vertexUV;

void main(){
    gl_FrontColor = texture2D(globeTexture,vertexUV)
}
