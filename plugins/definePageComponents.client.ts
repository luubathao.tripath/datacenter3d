import { defineNuxtPlugin } from 'nuxt/app'
import IndexPage from '../pages/threejs/index.vue'

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.component('IndexPage', IndexPage)
})
